package tigerio

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"

	"github.com/google/go-querystring/query"
)

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////               Client Configuration                     ////////////
////////                                                        ////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

type Client struct {
	url        string
	httpClient *http.Client
}

type Error struct {
	StatusCode int    `json:"code"`
	Message    string `json:"message"`
}

func (e Error) Error() string {
	return fmt.Sprintf("%d %s: %s", e.StatusCode, http.StatusText(e.StatusCode), e.Message)
}

func NewClient(url string, options ...func(*Client)) *Client {
	client := &Client{
		url:        url,
		httpClient: &http.Client{},
	}

	for _, option := range options {
		option(client)
	}

	return client
}

func WithHTTPClient(httpClient *http.Client) func(*Client) {
	return func(client *Client) {
		client.httpClient = httpClient
	}
}

func (c *Client) GetJSON(ctx context.Context, endpoint string, v interface{}) error {
	address, err := c.addEndpoint(endpoint)
	if err != nil {
		return err
	}
	data, err := c.getBytes(ctx, address)
	if err != nil {
		return err
	}
	return json.Unmarshal(data, v)
}

func (c *Client) GetBytes(ctx context.Context, endpoint string) ([]byte, error) {
	address, err := c.addEndpoint(endpoint)
	if err != nil {
		return []byte{}, err
	}
	return c.getBytes(ctx, address)
}

func (c *Client) getBytes(ctx context.Context, address string) ([]byte, error) {
	postEndpoints := `trade|modify|cancel`
	requestType := "GET"
	if yes, err := regexp.Match(postEndpoints, []byte(address)); err == nil && yes {
		requestType = "POST"
	} else if err != nil {
		return nil, err
	}
	req, err := http.NewRequest(requestType, address, nil)
	if err != nil {
		return []byte{}, err
	}
	resp, err := c.httpClient.Do(req.WithContext(ctx))
	if err != nil {
		return []byte{}, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		b, err := ioutil.ReadAll(resp.Body)
		msg := ""

		if err == nil {
			msg = string(b)
		}

		return []byte{}, Error{StatusCode: resp.StatusCode, Message: msg}
	}
	return ioutil.ReadAll(resp.Body)
}

func (c *Client) addEndpoint(endpoint string) (string, error) {
	u, err := url.Parse(c.url + endpoint)
	if err != nil {
		return "", err
	}
	return u.String(), nil
}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////               Trade Endpoints                          ////////////
////////                                                        ////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
func (c *Client) Assets() (Assets, error) {
	out := Assets{}
	endpoint := fmt.Sprintf("/assets")
	err := c.GetJSON(context.Background(), endpoint, &out)
	return out, err
}

func (c *Client) Positions() (Positions, error) {
	out := Positions{}
	endpoint := fmt.Sprintf("/positions")
	err := c.GetJSON(context.Background(), endpoint, &out)
	return out, err
}

func (c *Client) Orders(opts *RequestOptions) (Orders, error) {
	out := Orders{}
	endpoint := fmt.Sprintf("/orders")
	endpoint, err := c.ordersWithOpts(endpoint, opts)
	if err != nil {
		return nil, err
	}
	err = c.GetJSON(context.Background(), endpoint, &out)
	return out, err
}

func (c *Client) ordersWithOpts(endpoint string, opts *RequestOptions) (string, error) {
	if opts == nil {
		return endpoint, nil
	}
	v, err := query.Values(opts)
	if err != nil {
		return "", err
	}
	optParams := v.Encode()
	if optParams != "" {
		endpoint = fmt.Sprintf("%s?%s", endpoint, optParams)
	}
	return endpoint, nil
}

func (c *Client) OrdersByStatus(status OrderStatusType, opts *RequestOptions) (Orders, error) {
	out := Orders{}
	endpoint := fmt.Sprintf("/orders_by_status/%s", status)
	endpoint, err := c.ordersWithOpts(endpoint, opts)
	if err != nil {
		return nil, err
	}
	err = c.GetJSON(context.Background(), endpoint, &out)
	return out, err
}

func (c *Client) OrderByIDs(id, order_id int64) (Order, error) {
	out := Order{}
	endpoint := fmt.Sprintf("/order/%d/%d", id, order_id)
	err := c.GetJSON(context.Background(), endpoint, &out)
	return out, err
}

func (c *Client) PlaceOrder(symbol string, opts *RequestOptions) (Order, error) {
	out := Order{}
	endpoint := fmt.Sprintf("/trade/%s", symbol)
	endpoint, err := c.placeOrderWithOptions(endpoint, opts)
	if err != nil {
		return out, err
	}
	err = c.GetJSON(context.Background(), endpoint, &out)
	return out, err
}

func (c *Client) placeOrderWithOptions(endpoint string, opts *RequestOptions) (string, error) {
	if opts == nil {
		return endpoint, errors.New("missing order params")
	}
	if opts.Side != BUY && opts.Side != SELL {
		return endpoint, errors.New("missing order side")
	}
	if opts.Size <= 0 {
		return endpoint, errors.New("missing order size")
	}
	if opts.OrderType != MKT && opts.OrderType != LMT {
		return endpoint, errors.New("unsupported order type")
	}
	if opts.OrderType == LMT && opts.LimitPrice <= 0 {
		return endpoint, errors.New("missing limit price for a limit order")
	}
	v, err := query.Values(opts)
	if err != nil {
		return "", err
	}
	optParams := v.Encode()
	if optParams != "" {
		endpoint = fmt.Sprintf("%s?%s", endpoint, optParams)
	}
	return endpoint, nil
}

func (c *Client) ModifyOrder(ID, orderID int64, opts *RequestOptions) (Order, error) {
	out := Order{}
	endpoint := fmt.Sprintf("/modify/%d/%d", ID, orderID)
	endpoint, err := c.modifyOrderWithOptions(endpoint, opts)
	if err != nil {
		return out, err
	}
	err = c.GetJSON(context.Background(), endpoint, &out)
	return out, err
}

func (c *Client) modifyOrderWithOptions(endpoint string, opts *RequestOptions) (string, error) {
	if opts == nil {
		return endpoint, errors.New("missing order params")
	}
	if opts.Size <= 0 && opts.LimitPrice <= 0 {
		return endpoint, errors.New("missing order size or limit price")
	}
	v, err := query.Values(opts)
	if err != nil {
		return "", err
	}
	optParams := v.Encode()
	if optParams != "" {
		endpoint = fmt.Sprintf("%s?%s", endpoint, optParams)
	}
	return endpoint, nil
}

func (c *Client) CancelOrder(ID, orderID int64) (Order, error) {
	out := Order{}
	endpoint := fmt.Sprintf("/modify/%d/%d", ID, orderID)
	err := c.GetJSON(context.Background(), endpoint, &out)
	return out, err
}
